import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FirstTest {

    WebDriver driver;

@DataProvider(name = "urls")

    public static Object[][]urls(){

    return new Object[][]{
            { "https://www.grupobancolombia.com/wps/portal/personas", "homePage"},
    };
}


    @Test
    //Este test pcompara la imagen de homePage con la de SrcHomePAge
    public void compareImages()
    {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.grupobancolombia.com/wps/portal/personas");


        new ScreenCaptureUtility().takePageScreenshot(driver, "srcHomePage");
        Assert.assertTrue(new ScreenCaptureUtility().areImageEquals("homePage", "srcHomePage" ));

        driver.quit();
    }

    @Test
    // Este test crea una diferencia para probar correctamente

    public void compareImagesToFail()
    {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.grupobancolombia.com/wps/portal/personas");

        driver.findElement(By.xpath("//li[@id='transacciones']//a[contains(text(),'Transacciones')]")).click();

        new ScreenCaptureUtility().takePageScreenshot(driver, "srcHomePage");
        Assert.assertTrue(new ScreenCaptureUtility().areImageEquals("homePage", "srcHomePage" ));

        driver.quit();
    }

    @Test

    //Este test obtiene la pagina principal de Bancolombia

    public void test (){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
driver.get("https://www.grupobancolombia.com/wps/portal/personas");

new ScreenCaptureUtility().takePageScreenshot(driver, "myImage");


    }

@Test

        //Este Test permite obtener el logo de Bancolombia

    public void  test2(){

    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\drivers\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.get("https://www.grupobancolombia.com/wps/portal/personas");

    WebElement logo = driver.findElement(By.xpath("//a[@class='navbar-brand']//img[@id='logoFilial']"));
    new ScreenCaptureUtility().takeElementScreenshot(driver, "logoImage",logo);
}


}
